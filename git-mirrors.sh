#!/bin/bash
#Created by Sam Gleske
#MIT License
#Created Tue Sep 10 23:01:08 EDT 2013

#Include all user options and dependencies
git_mirrors_dir="${0%/*}"
[ -f "${git_mirrors_dir}/config.sh" ] && . "${git_mirrors_dir}/config.sh"
. "${git_mirrors_dir}/lib/VERSION"
. "${git_mirrors_dir}/lib/functions.sh"
if [ ! -f "${git_mirrors_dir}/config.sh" ];then
  red_echo "config.sh missing!  Copy and customize from config.sh.SAMPLE.  Aborting." 1>&2
  exit 1
fi

cd "${git_mirrors_dir}"

STATUS=0

export gitlab_namespace


ls -1 "${repo_dir}" | while read gitlab_namespace;do
    ls -1 "${repo_dir}/${gitlab_namespace}" | while read mirror;do
        if ! ./update_mirror.sh "${mirror}" "${gitlab_namespace}" &> /dev/null;then
            red_echo "Error: ./update_mirror.sh ${mirror} ${gitlab_namespace}" 1>&2
    	    STATUS=1
  	fi
    done
done
exit ${STATUS}
